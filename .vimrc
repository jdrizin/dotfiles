set nocompatible " be iMproved
" ============
" Vundle stuff
" ============
filetype off                   " required

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

 " let Vundle manage Vundle
 " required! 
Bundle 'gmarik/vundle'

 "my bundles
Bundle 'Lokaltog/vim-powerline'
Bundle 'altercation/vim-colors-solarized'
Bundle 'scrooloose/nerdcommenter'
Bundle 'stephenmckinney/vim-solarized-powerline'
Bundle 'tpope/vim-fugitive'
Bundle 'scrooloose/syntastic'

" ============================ 
" basic settings
syntax on
filetype on
filetype plugin on
filetype plugin indent on

" display
set number        " always show line numbers
set encoding=utf-8 " Necessary to show Unicode glyphs
set ruler           "show cursor everywhere
set colorcolumn=80

"moving and editing
set backspace=indent,eol,start
set showmatch     " set show matching parenthesis
set nowrap        " don't wrap lines
set linebreak     " and not in the middle of a word
set autoindent    " always set autoindenting on
set smartindent   " use smart indent if there is no indent file
set copyindent    " copy the previous indentation on autoindenting
set tabstop=4     " a tab is four spaces
set expandtab     " use spaces for tabs
set softtabstop=4
set shiftwidth=4
set smarttab      " insert tabs on the start of a line according to
                  "    shiftwidth, not tabstop
set shiftround    " rounds an indent to multiples of shiftwidth
set matchpairs+=<:> "match for <> for html
set foldmethod=indent "fold on indents
set foldlevel=99 "don't autofold


 " searching settings 
set smartcase     " ignore case if search pattern is all lowercase,
                  "    case-sensitive otherwise
set hlsearch      " highlight search terms

 " dealing with backups
set nobackup      " what is this, 1970?
set noswapfile

"============================
" syntastic configuration
let g:syntastic_check_on_open=1
let g:syntastic_python_checker="pyflakes"

"============================
" this is for powerline

let g:Powerline_symbols = 'fancy'

set laststatus=2   " Always show the statusline

 " and vim colors
let g:Powerline_colorscheme='solarized16'

"=========================
"personal bindings
"========================
 " make pastemode fine
set pastetoggle=<F2>

 " make f12 toggle
:nmap <F12> :set invnumber<CR>

